/*
 * Copyright © 2022 UBports foundation.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authored by: Alexander Martinz <amartinz@shiftphones.com>
 */

#pragma once

#include "src/adapters/filesystem.h"
#include "src/core/log.h"

#include <deviceinfo/deviceinfo.h>

#include <memory>
#include <string>
#include <vector>

namespace repowerd
{
class Log;
class Filesystem;

class DoubleTapToWake
{
public:
    virtual ~DoubleTapToWake() = default;

    virtual bool is_enabled() = 0;
    virtual bool is_supported() = 0;

    virtual void enable() = 0;
    virtual void disable() = 0;

protected:
    DoubleTapToWake() = default;
    DoubleTapToWake (DoubleTapToWake const&) = default;
    DoubleTapToWake& operator=(DoubleTapToWake const&) = default;

    std::shared_ptr<Log> const log;
    std::shared_ptr<Filesystem> const filesystem;

    virtual void set_enabled(bool) = 0;
};

}