/*
 * Copyright © 2022 UBports foundation.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authored by: Alexander Martinz <amartinz@shiftphones.com>
 */

#pragma once

#include "src/adapters/fs_double_tap_to_wake.h"

namespace repowerd
{

namespace test
{

class FakeDoubleTapToWake : public repowerd::FsDoubleTapToWake
{
public:
    FakeDoubleTapToWake(
        std::shared_ptr<Log> const& log,
        std::shared_ptr<Filesystem> const& filesystem);

    bool is_enabled() { return false; };
    bool is_supported() { return false; };

    void enable() { /* do nothing */ };
    void disable() { /* do nothing */ };

protected:
    void set_enabled(bool) { /* do nothing */ };
};

}

}