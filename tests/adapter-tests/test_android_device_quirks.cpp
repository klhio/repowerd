/*
 * Copyright © 2016 Canonical Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authored by: Alexandros Frantzis <alexandros.frantzis@canonical.com>
 */

#include "src/adapters/android_device_quirks.h"

#include "fake_device_info.h"
#include "fake_log.h"
#include "fake_filesystem.h"

#include <gtest/gtest.h>

using namespace testing;
namespace rt = repowerd::test;

namespace
{

struct AnAndroidDeviceQuirks : Test
{
    std::shared_ptr<rt::FakeDeviceInfo> fake_device_info{std::make_shared<rt::FakeDeviceInfo>()};
    rt::FakeLog fake_log{repowerd::LogLevel::Debug};
};

}

TEST_F(AnAndroidDeviceQuirks,
       ignore_session_deactivation_is_false_if_device_is_desktop)
{
    fake_device_info->set_is_desktop(true);
    repowerd::AndroidDeviceQuirks android_device_quirks{fake_log, fake_device_info};

    EXPECT_FALSE(android_device_quirks.ignore_session_deactivation());
    EXPECT_TRUE(fake_log.contains_line({"ignore_session_deactivation", "false"}));
}

TEST_F(AnAndroidDeviceQuirks,
       ignore_session_deactivation_is_true_if_device_is_not_desktop)
{
    repowerd::AndroidDeviceQuirks android_device_quirks{fake_log, fake_device_info};

    EXPECT_TRUE(android_device_quirks.ignore_session_deactivation());
    EXPECT_TRUE(fake_log.contains_line({"ignore_session_deactivation", "true"}));
}
